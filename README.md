# Example data to add:
```
curl --header "Content-Type: application/json"  --request POST --data '{"name":"x-company", "address":{"street":"Green","city":"Labrock","country":"USA"}, "employees":[{"firstNae":"Alan", "lastName":"Klark"}, {"firstName":"Issak", "lastName":"Trukoy"}]}' 'http://localhost:8080/companies'
```

# Example get data request and response:
```
curl http://localhost:8080/companies/1
{
  "id" : 1,
  "name" : "x-company",
  "address" : {
    "idAddress" : 1,
    "street" : "Green",
    "city" : "Labrock",
    "country" : "USA"
  },
  "employees" : [ {
    "employeeId" : 1,
    "firstName" : "Alan",
    "lastName" : "Klark",
    "_links" : {
      "company" : {
        "href" : "http://localhost:8080/companies/1"
      }
    }
  }, {
    "employeeId" : 2,
    "firstName" : "Issak",
    "lastName" : "Trukoy",
    "_links" : {
      "company" : {
        "href" : "http://localhost:8080/companies/1"
      }
    }
  } ],
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/companies/1"
    },
    "company" : {
      "href" : "http://localhost:8080/companies/1"
    }
  }
}
```