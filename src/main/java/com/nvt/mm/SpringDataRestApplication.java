package com.nvt.mm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import com.nvt.mm.domain.Address;
import com.nvt.mm.domain.Company;
import com.nvt.mm.domain.Employee;

@SpringBootApplication
public class SpringDataRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataRestApplication.class, args);
    }

    @Configuration
    @Import(RepositoryRestMvcConfiguration.class)
    public static class ExposeEntityIdRestConfiguration extends RepositoryRestConfigurerAdapter {

        @Override
        public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
            config.exposeIdsFor(Company.class);
            config.exposeIdsFor(Employee.class);
            config.exposeIdsFor(Address.class);
        }

    }

}
