package com.nvt.mm.repository;

import org.springframework.data.repository.CrudRepository;

import com.nvt.mm.domain.Company;

public interface CompanyRepository extends CrudRepository<Company, Integer> {
}
